from main import greeting

import unittest

class TestMain(unittest.TestCase):
  def test_greeting(self):
    self.assertEqual(greeting('Joe'), 'Hello, Joe')

if __name__ == '__main__':
    unittest.main()
